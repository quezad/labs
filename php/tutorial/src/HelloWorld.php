<?php
declare(strict_types=1);

namespace ExampleApp;

use Psr\Http\Message\ResponseInterface;

/**
 * 
 * @package ExampleApp
 */
class HelloWorld{

    /**
     * @var string
     */
    private $name;
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * HelloWorld constructor.
     * 
     * @param ResponseInterface $response
     * @param string $name
     */
    public function __construct(
        string $name,
        ResponseInterface $response) {
            $this->name = $name;
            $this->response = $response;
    }

    /**
     * 
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function __invoke(): ResponseInterface {
        $response = $this->response
            ->withHeader('Content-Type', 'text/html');
        $response->getBody()
            ->write("
                <html>
                    <head></head>
                    <body>
                        Hello, {$this->name} world!
                    </body>
                </html>
            ");

        return $response;
    }
}
?>