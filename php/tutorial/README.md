# The very start of the tutorial: PHP

This is a very simple project meant to get you started with PHP development, I'm using **[VS Code Remote - Containers](https://aka.ms/vscode-remote/containers)** extension to not polute my system.

Please bring this repository to life with more and more code as we evolve it to the final project.

> **Note:** If you're using a local installation please skip to the [Things to try](#things-to-try) section.

## Setting up the development container

Follow these steps to open this sample in a container:

1. If this is your first time using a development container, please follow the [getting started steps](https://aka.ms/vscode-remote/containers/getting-started).

2. To use this repository open a locally cloned copy of the code:

   - Clone this repository to your local filesystem.
   - Press <kbd>F1</kbd> and select the **Remote-Containers: Open Folder in Container...** command.
   - Select the cloned copy of this folder, wait for the container to start, and try things out!

> **Note:** As the project evolves, it might require separate configurations to running it locally or inside a container, we will try to keep this update, and independent of the development environment.

## Things to try

Once you have this sample opened in a container, you'll be able to work with it like you would locally.

> **Note:** This container runs as a non-root user with sudo access by default. Comment out `"remoteUser": "vscode"` in `.devcontainer/devcontainer.json` if you'd prefer to run as root.

Some things to try:

1. **Edit:**
   - Open `index.php`
   - Try adding some code and check out the language features.
1. **Terminal:** Press <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>\`</kbd> and type `uname` and other Linux commands from the terminal window.
1. **Run and Debug:**
   - Open `index.php`
   - Add a breakpoint (e.g. on line 4).
   - Press <kbd>F5</kbd> to launch the app in the container.
   - Once the breakpoint is hit, try hovering over variables, examining locals, and more.
1. **Running a server:**
   - From the terminal, run `php -S 0.0.0.0:8000`
   - Press <kbd>F1</kbd> and run the **Forward a Port** command.
   - Select port `8000`.
   - Click "Open Browser" in the notification that appears to access the web app on this new port.
   - Look back at the terminal, and you should see the output from your site navigations
   - Edit the text on line 21 in `index.php` and refresh the page to see the changes immediately take affect
1. **Attach debugger to the server:**
   - Follow the previous steps to start up a PHP server and open a browser on port `8000`
   - Press <kbd>F1</kbd> and select the **View: Show Debug** command
   - Pick "Listen for XDebug" from the dropdown
   - Press <kbd>F5</kbd> to attach the debugger
   - Add a breakpoint to `index.php` if you haven't already
   - Reload your browser window
   - Once the breakpoint is hit, try hovering over variables, examining locals, and more.

## Resources on the web

Please refer to the following list for additional information, guides, HowTos, getting started, etc. Give this list some love and keep it growing:

- [<? PHP: The Right Way](https://phptherightway.com/)
- [Official getting started with PHP](https://www.php.net/manual/en/getting-started.php)